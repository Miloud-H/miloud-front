import {SkillsPage} from './pages/skills.po';
import { browser } from 'protractor';

describe('Section Compétences', () => {
  let page: SkillsPage;

  beforeEach(() => {
    page = new SkillsPage();
    browser.get('/#skills');
  });

  it('should render title in a h2 tag', () => {
    expect(page.skillTitle().getText()).toEqual('MES COMPÉTENCES');
  });

  it('should dev skills have developpement title', () => {
    expect(page.devListTitle().getText()).toEqual('DÉVELOPPEMENT');
  });

  it('should operational skills have operational title', () => {
    expect(page.operationalListTitle().getText()).toEqual('OPÉRATIONEL');
  });

  it('should have a list of 7 dev skills', () => {
    expect(page.devListNumber()).toEqual(7);
  });

  it('should have a list of 4 operational skills', () => {
    expect(page.operationalListNumber()).toEqual(4);
  });

});
