import { MainPage } from './pages/main.po';
import { browser, logging } from 'protractor';

describe('Section Principale', () => {
  let page: MainPage;

  beforeEach(() => {
    page = new MainPage();
    browser.get('/#main');

  });

  it('should render title in a h2 tag', () => {
    expect(page.welcomeTitle().getText()).toContain('SALUT, JE SUIS MILOUD');
  });

  it('should have a p to explain about me', () => {
    expect(page.explainAboutMe().getText()).toBe('Développeur back-end Symfony/Laravel');
  });

  it('should have a button to go to about me section', () => {
    expect(page.moreInfoButton().getAttribute('href')).toContain('#about-me');
  });

  it('should have a button to go to about me section with text', () => {
    expect(page.moreInfoButton().getText()).toBe('PLUS D\'INFORMATIONS');
  });

  it('should have an img', () => {
    expect(page.imageOfMe()).toBeDefined();
  });
});
