import { browser, by, element } from 'protractor';

export class ContactPage {

  contactTitle() {
    return element(by.css('#contact h2'));
  }

  nameInput() {
    return element(by.css('#contact #name'));
  }

  nameInputError() {
    return element(by.css('#contact #name ~ .invalid-feedback'));
  }

  mailInput() {
    return element(by.css('#contact #email'));
  }

  mailInputError() {
    return element(by.css('#contact #email ~ .invalid-feedback'));
  }

  phoneInput() {
    return element(by.css('#contact #phone'));
  }

  phoneInputError() {
    return element(by.css('#contact #phone ~ .invalid-feedback'));
  }

  subjectInput() {
    return element(by.css('#contact #subject'));
  }

  subjectInputError() {
    return element(by.css('#contact #subject ~ .invalid-feedback'));
  }

  messageInput() {
    return element(by.css('#contact #message'));
  }

  messageInputError() {
    return element(by.css('#contact #message ~ .invalid-feedback'));
  }

  submitButton() {
    return element(by.css('#contact button'));
  }
}
