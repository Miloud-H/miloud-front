import { browser, by, element } from 'protractor';

export class SkillsPage {

  devListTitle() {
    return element.all(by.css('#skills .card-title')).get(0);
  }

  operationalListTitle() {
    return element.all(by.css('#skills .card-title')).get(1);
  }

  devListNumber() {
    return element.all(by.css('#skills ul')).get(0).all(by.tagName('li')).count();
  }

  operationalListNumber() {
    return element.all(by.css('#skills ul')).get(1).all(by.tagName('li')).count();
  }

  skillTitle() {
    return element(by.css('#skills h2'));
  }
}
