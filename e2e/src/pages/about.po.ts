import { browser, by, element } from 'protractor';

export class AboutPage {

  aboutTitle() {
    return element(by.css('#about-me h2'));
  }

  aboutDescription() {
    return element(by.css('#about-me p'));
  }

  aboutImage() {
    return element(by.css('#about-me img'));
  }

  hireButton() {
    return element(by.css('#about-me a'));
  }

  downloadButton() {
    return element(by.css('#about-me button'));
  }
}
