import { browser, by, element } from 'protractor';

export class ProjectsPage {

  projectsTitle() {
    return element(by.css('#projects h2'));
  }

  carousel() {
    return element(by.css('#projects p-carousel'));
  }
}
