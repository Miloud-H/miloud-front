import { browser, by, element } from 'protractor';

export class MainPage {

  welcomeTitle() {
    return element(by.css('#main h2'));
  }

  explainAboutMe() {
    return element(by.css('#main p'));
  }

  moreInfoButton() {
    return element(by.css('#main a'));
  }

  imageOfMe() {
    return element(by.css('#main img'));
  }
}
