import { AboutPage } from './pages/about.po';
import { browser, logging } from 'protractor';

describe('Section A Propos', () => {
  let page: AboutPage;

  beforeEach(() => {
    page = new AboutPage();
    browser.get('/#about-me');

  });

  it('should have h2 with Who i am', () => {
    expect(page.aboutTitle().getText()).toBe('QUI JE SUIS ?');
  });

  it('should have a p with description about me', () => {
    expect(page.aboutDescription().getText().then(v => v.length)).toBeGreaterThan(0);
  });

  it('should have image', () => {
    expect(page.aboutImage().getAttribute('src')).toBeDefined();
  });

  it('should have hire me button', () => {
    expect(page.hireButton().getText()).toBe('M\'ENGAGER');
  });

  it('should have download CV button', () => {
    expect(page.downloadButton().getText()).toBe('TÉLÉCHARGER MON CV');
  });

});
