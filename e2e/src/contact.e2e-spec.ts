import {ContactPage} from './pages/contact.po';
import {browser, logging, protractor} from 'protractor';

describe('Section Contact', () => {
  let page: ContactPage;

  beforeAll( () => {
    browser.waitForAngularEnabled(true);
  });

  beforeEach(() => {
    page = new ContactPage();
    browser.get('/#contact');
  });

  it('should have h2 with my projects', () => {
    expect(page.contactTitle().getText()).toBe('ME CONTACTER');
  });

  it('should have a name input', () => {
    expect(page.nameInput().getAttribute('placeholder')).toEqual('Nom');
  });

  it('should have a input name error if empty', () => {
    page.nameInput().sendKeys(protractor.Key.ENTER);
    expect(page.nameInput().getAttribute('class')).toContain('is-invalid');
    expect(page.nameInputError().getText()).toEqual('Vous devez entrer un nom');
    page.nameInput().sendKeys('toto');
    expect(page.nameInput().getAttribute('class')).toContain('is-valid');
  });

  it('should have a email input', () => {
    expect(page.mailInput().getAttribute('placeholder')).toEqual('Email');
  });

  it('should have a input mail error if empty', () => {
    page.mailInput().sendKeys(protractor.Key.ENTER);
    expect(page.mailInput().getAttribute('class')).toContain('is-invalid');
    expect(page.mailInputError().getText()).toEqual('Vous devez entrer une adresse mail valide');
  });

  it('should have a input mail error if wrong format', () => {
    page.mailInput().sendKeys('toto');
    page.mailInput().sendKeys(protractor.Key.ENTER);
    expect(page.mailInput().getAttribute('class')).toContain('is-invalid');
    expect(page.mailInputError().getText()).toEqual('Vous devez entrer une adresse mail valide');
  });

  it('should have a not input mail error', () => {
    page.mailInput().sendKeys('toto@toto.com');
    expect(page.mailInput().getAttribute('class')).toContain('is-valid');
  });

  it('should have a phone input', () => {
    expect(page.phoneInput().getAttribute('placeholder')).toEqual('Téléphone');
  });

  it('should have a input phone error if wrong format', () => {
    page.phoneInput().sendKeys('0000');
    page.phoneInput().sendKeys(protractor.Key.ENTER);
    expect(page.phoneInput().getAttribute('class')).toContain('is-invalid');
    expect(page.phoneInputError().getText()).toEqual('Votre numéro de téléphone n\'est pas valide');
  });

  it('should have a not input phone error', () => {
    page.phoneInput().sendKeys('0600000000');
    expect(page.phoneInput().getAttribute('class')).toContain('is-valid');
  });

  it('should have a subject input', () => {
    expect(page.subjectInput().getAttribute('placeholder')).toEqual('Sujet');
  });

  it('should have a input subject error if empty', () => {
    page.subjectInput().sendKeys(protractor.Key.ENTER);
    expect(page.subjectInput().getAttribute('class')).toContain('is-invalid');
    expect(page.subjectInputError().getText()).toEqual('Veuillez entrer un sujet valide');
  });

  it('should have a message input', () => {
    expect(page.messageInput().getAttribute('placeholder')).toEqual('Votre message');
  });

  it('should have a input message error if empty', () => {
    page.submitButton().sendKeys(protractor.Key.ENTER);
    expect(page.messageInput().getAttribute('class')).toContain('is-invalid');
    expect(page.messageInputError().getText()).toEqual('Vous ne pouvez pas envoyer de message vide');
  });

  it('should have a submit button', () => {
    expect(page.submitButton().getText()).toEqual('ENVOYER');
  });

});
