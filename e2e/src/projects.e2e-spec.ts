import { ProjectsPage } from './pages/projects.po';
import { browser, logging } from 'protractor';

describe('Section Projets', () => {
  let page: ProjectsPage;

  beforeEach(() => {
    page = new ProjectsPage();
    browser.get('/#projects');

  });

  it('should have h2 with my projects', () => {
    expect(page.projectsTitle().getText()).toBe('MES RÉALISATIONS');
  });

  it('should have a carousel', () => {
    expect(page.carousel()).toBeDefined();
  });

});
