export const environment = {
  production: true,
  reCaptchaPublic: '6Ld98OsUAAAAAPPcUrbvpiGGmD5vVIPKO_u3OEXH',
  reCaptchaPrivate: '6Ld98OsUAAAAACNQs-rGWFrZFJDmaNFeTg1laFyy',
  api_url: 'https://api.miloud.dev',
  api_sufix: '.jsonld'
};
