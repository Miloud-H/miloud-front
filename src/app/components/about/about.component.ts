import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  constructor(private api: ApiService) { }

  ngOnInit() {
  }

  download() {
    this.api.downloadCV().subscribe(res => {
      const blob = new Blob([res], { type : 'application/pdf' });
      const file = new File([blob], 'CV_HABOUDOU_Miloud.pdf', { type: 'application/pdf' });
      saveAs(file);
    });
  }

}
