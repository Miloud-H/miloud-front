import { Component, OnInit } from '@angular/core';
import {faLinkedin, faTwitter, faViadeo} from '@fortawesome/free-brands-svg-icons';
import {faCircle, faGlobe} from '@fortawesome/free-solid-svg-icons';
import {icon} from '@fortawesome/fontawesome-svg-core';
// import {faCircle} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  icons;

  constructor() { }

  ngOnInit() {
    this.icons = {
      twitter: faTwitter,
      linkedin: faLinkedin,
      viadeo: faViadeo,
      website: faGlobe,
      circle: faCircle
    };
  }

}
