import {Component, OnInit, TemplateRef} from '@angular/core';
import { faLink, faSearch } from '@fortawesome/free-solid-svg-icons';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {Project} from '../../models/Project';
import {Restangular} from 'ngx-restangular';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects: Project[];
  project: Project;
  modalRef: BsModalRef;
  responsiveOption: object[];
  icons;

  constructor(private modalService: BsModalService, private api: Restangular) { }

  ngOnInit() {
    this.icons = {
      link: faLink,
      search: faSearch
    };

    this.api.all('projects').getList().subscribe(projects => {
      this.projects = projects;
    });

    this.responsiveOption = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }

  details(modal: TemplateRef<any>, project: Project): any {
    this.project = project;
    this.modalRef = this.modalService.show(modal);
  }

}
