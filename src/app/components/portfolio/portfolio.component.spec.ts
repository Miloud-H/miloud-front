import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioComponent } from './portfolio.component';
import {MainComponent} from '../main/main.component';
import {NavbarComponent} from '../navbar/navbar.component';
import {AboutComponent} from '../about/about.component';
import {ProjectsComponent} from '../projects/projects.component';
import {FooterComponent} from '../footer/footer.component';
import {CarouselModule} from 'primeng/carousel';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ContactComponent} from '../contact/contact.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module} from 'ng-recaptcha';
import {environment} from '../../../environments/environment';
import {ModalModule} from 'ngx-bootstrap/modal';
import {SkillsComponent} from '../skills/skills.component';
import {RestangularModule} from 'ngx-restangular';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CollapseModule} from 'ngx-bootstrap/collapse';

export function RestangularConfigFactory(RestAngularProvider) {
  RestAngularProvider.setBaseUrl(environment.api_url);
  RestAngularProvider.setRequestSuffix(environment.api_sufix);
  RestAngularProvider.setRestangularFields({
    id: '@id',
    selfLink: '@id'
  });
  RestAngularProvider.setSelfLinkAbsoluteUrl(false);
  RestAngularProvider.addResponseInterceptor((datas, operation) => {
    // Remove trailing slash to make Restangular working
    function populateHref(data) {
      if (data['@id']) {
        data.href = data['@id'].substring(1);
      }
    }

    // Populate href property for the collection
    populateHref(datas);

    if ('getList' === operation) {
      const collectionResponse = datas['hydra:member'];
      collectionResponse.metadata = {};

      // Put metadata in a property of the collection
      Object.entries(datas).forEach((value: [string, any]) => {
        if ('hydra:member' !== value[0]) {
          collectionResponse.metadata[value[0]] = value[1];
        }
      });

      // Populate href property for all elements of the collection
      Object.entries(collectionResponse).forEach((value: [string, any]) => {
        populateHref(value[1]);
      });

      return collectionResponse;
    }

    return datas;
  });

}

describe('PortfolioComponent', () => {
  let component: PortfolioComponent;
  let fixture: ComponentFixture<PortfolioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CarouselModule,
        FontAwesomeModule,
        ReactiveFormsModule,
        RecaptchaV3Module,
        RestangularModule.forRoot(RestangularConfigFactory),
        CollapseModule.forRoot(),
        BrowserAnimationsModule,
        ModalModule.forRoot()
      ],
      declarations: [
        PortfolioComponent,
        NavbarComponent,
        MainComponent,
        AboutComponent,
        SkillsComponent,
        ProjectsComponent,
        ContactComponent,
        FooterComponent
      ],
      providers: [
        { provide: RECAPTCHA_V3_SITE_KEY, useValue: environment.reCaptchaPublic }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
