import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainComponent } from './main.component';

describe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*it('should be moved to about me section', () => {
    const compiled = fixture.debugElement;
    fixture.detectChanges();
    const button = compiled.query(By.css('a'));

    button.triggerEventHandler('click', {});
    fixture.detectChanges();

    expect(compiled.nativeElement.getBoundingClientRect().bottom).toBeLessThan(0);
  });*/
});
