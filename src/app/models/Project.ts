import {Technology} from './Technology';

export class Project {
  name: string;
  description: string;
  image: string;
  url: string;
  stack: Technology[];
}
