import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './components/main/main.component';
import { AboutComponent } from './components/about/about.component';
import {AccordionModule} from 'primeng/accordion';
import { ProjectsComponent } from './components/projects/projects.component';
import {CarouselModule} from 'primeng/carousel';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FooterComponent } from './components/footer/footer.component';
import { ContactComponent } from './components/contact/contact.component';
import {RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module} from 'ng-recaptcha';
import { environment } from '../environments/environment';
import {ReactiveFormsModule} from '@angular/forms';
import {RestangularModule} from 'ngx-restangular';
import {SkillsComponent} from './components/skills/skills.component';
import {CollapseModule} from 'ngx-bootstrap/collapse';

export function RestangularConfigFactory(RestAngularProvider) {
  RestAngularProvider.setBaseUrl(environment.api_url);
  RestAngularProvider.setRequestSuffix(environment.api_sufix);
  RestAngularProvider.setRestangularFields({
    id: '@id',
    selfLink: '@id'
  });
  RestAngularProvider.setSelfLinkAbsoluteUrl(false);
  RestAngularProvider.addResponseInterceptor((datas, operation) => {
    // Remove trailing slash to make Restangular working
    function populateHref(data) {
      if (data['@id']) {
        data.href = data['@id'].substring(1);
      }
    }

    // Populate href property for the collection
    populateHref(datas);

    if ('getList' === operation) {
      const collectionResponse = datas['hydra:member'];
      collectionResponse.metadata = {};

      // Put metadata in a property of the collection
      Object.entries(datas).forEach((value: [string, any]) => {
        if ('hydra:member' !== value[0]) {
          collectionResponse.metadata[value[0]] = value[1];
        }
      });

      // Populate href property for all elements of the collection
      Object.entries(collectionResponse).forEach((value: [string, any]) => {
        populateHref(value[1]);
      });

      return collectionResponse;
    }

    return datas;
  });

}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PortfolioComponent,
    MainComponent,
    AboutComponent,
    ProjectsComponent,
    FooterComponent,
    ContactComponent,
    SkillsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AccordionModule,
    CarouselModule,
    CollapseModule.forRoot(),
    FontAwesomeModule,
    RecaptchaV3Module,
    ReactiveFormsModule,
    RestangularModule.forRoot(RestangularConfigFactory),
    ModalModule.forRoot()
  ],
  providers: [
    { provide: RECAPTCHA_V3_SITE_KEY, useValue: environment.reCaptchaPublic }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
